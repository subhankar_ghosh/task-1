package com.subhankar.task1

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class InstalledAppListRecyclerViewAdapter(val context:Context,private val arrayList: ArrayList<PackageInfo>)
    : RecyclerView.Adapter<InstalledAppListRecyclerViewAdapter.RecyclerViewAdapterViewHolder>(),Filterable{

    var appsFilterList = ArrayList<PackageInfo>()

    init {
        appsFilterList = arrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapterViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.list_row_item, parent, false)

        return RecyclerViewAdapterViewHolder(
            itemView
        )
    }

    override fun getItemCount(): Int {
        return appsFilterList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapterViewHolder, position: Int) {

        val model = appsFilterList[position]

        holder.appNameTv?.text = model.applicationInfo.loadLabel(context.packageManager).toString()
        holder.appPackageNameTv?.text = model.packageName
        holder.versionCodeTv?.text = model.versionCode.toString()
        holder.versionNameTv?.text = model.versionName
        holder.tvMainActivity?.text = context.packageManager.getLaunchIntentForPackage(model.packageName.toString())?.component?.className?.substringAfterLast('.')

        val appIcon = model.applicationInfo.loadIcon(context.packageManager)

        holder.appLauncherIconIv?.setImageDrawable(appIcon)




        holder.mainCV?.setOnClickListener {

            val intent: Intent? = context.packageManager.getLaunchIntentForPackage(model.packageName)
            if (intent != null){
                context.startActivity(intent)
            }else{
                Toast.makeText(context,"This is not a standalone app to open",Toast.LENGTH_SHORT).show()
            }
        }



    }

    class RecyclerViewAdapterViewHolder(view:View) :RecyclerView.ViewHolder(view) {

        internal var mainCV:CardView? = null
        internal var appNameTv:TextView? = null
        internal var appPackageNameTv:TextView? = null
        internal var mainActivityClassNameTv:TextView? = null
        internal var versionCodeTv:TextView? = null
        internal var versionNameTv:TextView? = null
        internal var tvMainActivity:TextView? = null
        internal var appLauncherIconIv:ImageView? = null


        init {
            mainCV = view.findViewById(R.id.mainCv)
            appNameTv = view.findViewById(R.id.appNameTv)
            appPackageNameTv = view.findViewById(R.id.appPackageNameTv)
            mainActivityClassNameTv = view.findViewById(R.id.mainActivityClassNameTv)
            versionCodeTv = view.findViewById(R.id.versionCodeTv)
            versionNameTv = view.findViewById(R.id.versionNameTv)
            appLauncherIconIv = view.findViewById(R.id.appLauncherIconIv)
            tvMainActivity = view.findViewById(R.id.tvMainActivity)
        }


    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    appsFilterList = arrayList as ArrayList<PackageInfo>
                } else {
                    val resultList = ArrayList<PackageInfo>()
                    for (row in arrayList) {
                        if (row.applicationInfo.loadLabel(context.packageManager).toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            resultList.add(row)
                        }
                    }
                    appsFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = appsFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                appsFilterList = results?.values as ArrayList<PackageInfo>
                notifyDataSetChanged()
            }
        }
    }

}




