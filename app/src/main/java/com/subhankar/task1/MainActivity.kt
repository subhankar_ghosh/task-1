package com.subhankar.task1

import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.subhankar.task1.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val TAG:String? = MainActivity::class.simpleName
    private lateinit var mainActivityBinding:ActivityMainBinding
    private lateinit var installedAppListRecyclerViewAdapter: InstalledAppListRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        mainActivityBinding =DataBindingUtil.setContentView(this,R.layout.activity_main)


        /**
         * get a list of installed apps.
         */
        val packageList:List<PackageInfo> = packageManager.getInstalledPackages(0)
        val installedApps = mutableListOf<PackageInfo>()
        for (i in packageList.indices) {
            val packageInfo = packageList[i]
            if (packageInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM == 0) {
                installedApps.add(packageInfo)
            }
        }
        installedApps.sortBy { it.applicationInfo.loadLabel(packageManager).toString() }
        //installedApps.map { println("\n@@@ "+packageManager.getLaunchIntentForPackage(it.packageName.toString())?.component?.className?.substringAfterLast('.')) }



        /**
         * Initialize and Set RecyclerView Adapter Of Installed App Listing
         */
        mainActivityBinding.appListRv.layoutManager =  LinearLayoutManager(this)
        installedAppListRecyclerViewAdapter = InstalledAppListRecyclerViewAdapter(this,installedApps as ArrayList<PackageInfo>)
        mainActivityBinding.appListRv.adapter = installedAppListRecyclerViewAdapter


        //appSearchEt.onActionViewExpanded()
        //appSearchEt.isIconified = true
        appSearchEt.setOnQueryTextListener(object  : android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                installedAppListRecyclerViewAdapter.filter.filter(query)
                return true
            }

        })

    }
}